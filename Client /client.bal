import ballerinax/kafka;
import ballerina/io;
import ballerina/uuid;

kafka:ConsumerConfiguration consumerConfiguration = {
    groupId: "1",
    offsetReset: "earliest",
    topics: ["serverResponse"],
    autoCommit: false
};
json loggedOn = {};
json cours = {};

type loggedIn record {|
    string id;
    string username;
    string firstname;
    string lastname?;
    string user_type;
    json[] courses;
|};

type Course record {|
    string id;
    string course_name;
    string course_code;
    json[] course_outline;
    boolean approved;
|};

type loggedInArray loggedIn[];

kafka:Consumer consumer = check new (kafka:DEFAULT_URL, consumerConfiguration);

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    string uuid1String = uuid:createType1AsString();
    boolean lcv = true;
    json[] courses = [];

    //=====================read user==================================
    json b1 = {opr: "r", _type: "user", key: "Sem_Illeka"};
    // error? readResult = send1(b1);

    //=====================write new user===============================

    //=====================write new Course===============================

    json|error? readResult = send1(b1);
    io:println(readResult);
    if (readResult == "" || readResult == {}) {
        io:println("no user like that in the system");
    } else {
        // io:println("res:", readResult);
        loggedOn = check readResult;
    }
    while loggedOn != {} {
        io:println("Hello ", loggedOn.name, " your menu is bellow");
        if (loggedOn.user_type == "student") {
            while lcv == true {
                string choice = "";
                choice = io:readln("================= Student Menu =======================\n1:View a course outline\n2:acknowledge reception\n3:Logout\n");
                match choice {
                    "1" => {
                        io:println("Your course are\n", loggedOn.courses);
                        string courName = io:readln("================= Enter subject name =======================\n");
                        json courseO = {opr: "r", _type: "course", key: courName};
                        json|error? courseOutLine = send1(courseO);
                        cours = check courseOutLine;

                        io:println("The course outline for this module is: \n", cours.course_outline, "\nStatus of signature:", cours.signed);
                    }
                    "2" => {
                        string courName = io:readln("================= Enter subject name =======================\n");
                        json courseO = {opr: "r", _type: "course", key: courName};
                        json|error? courseOutLine = send1(courseO);
                        cours = check courseOutLine;
                        io:println("The module details are below: \n", cours.course_outline, "\nStatus of signature:", cours.signed);
                        string signcourse = "";
                        boolean signcourselcv = true;
                        boolean userResSign = false;
                        while signcourselcv == true {
                            signcourse = io:readln("Are you willing to sign the course outline?\n1:To sign\n2:To reject the course outline\n");
                            if signcourse == "1" {
                                signcourselcv = false;
                                userResSign = true;
                            }
                            if signcourse == "2" {
                                signcourselcv = false;
                                userResSign = false;
                            }
                        }
                        if signcourse != "" {
                            json sendSig = {opr: "upd", _type: "sign_course", _data: userResSign, key: courName, user_id: check loggedOn.username};
                            io:println(sendSig);
                            json|error? sendSigres = send1(sendSig);
                        }
                    }
                    "3" => {
                        lcv = false;
                    }

                }

            }
        }
        if (loggedOn.user_type == "lecture") {
            while lcv == true {
                string choice = "";
                choice = io:readln("================= Lecture Menu =======================\n1:View a course outline\n2:input information about a course he/she has been assigned\n3:generate and sign a course outline for a course he/she has been assigned\n4:Logout\n");

                match choice {
                    "1" => {
                        io:println("Your course are\n", loggedOn.courses);
                        string courName = io:readln("================= Enter subject name =======================\n");
                        json courseO = {opr: "r", _type: "course", key: courName};
                        json|error? courseOutLine = send1(courseO);
                        cours = check courseOutLine;

                        io:println("The course outline for this module is: \n", cours.course_outline, "\nStatus of signature:", cours.signed);
                    }
                    "2" => {
                        string courName = io:readln("================= Enter subject name =======================\n");
                        json courseO = {opr: "r", _type: "course", key: courName};
                        json|error? courseOutLine = send1(courseO);
                        cours = check courseOutLine;
                        io:println(cours);
                        io:println("The module details are below: \n", cours.course_outline, "\nStatus of signature:", cours.approved);
                        string coursenewInfo = io:readln("Add the additional information to the course outline\n");

                        json sendSig = {_fieldToUpdate: "course_outline", opr: "upd", _type: "add_info", _data: coursenewInfo, key: courName, user_id: check loggedOn.username, user_level: check loggedOn.user_type};
                        io:println(sendSig);
                        json|error? sendSigres = send1(sendSig);

                    }
                    "3" => {
                        string courName = io:readln("================= Enter subject name =======================\n");
                        json courseO = {opr: "r", _type: "course", key: courName};
                        json|error? courseOutLine = send1(courseO);
                        cours = check courseOutLine;
                        io:println("The module details are below: \n", cours.course_outline, "\nStatus of signature:", cours.signed);
                        string signcourse = "";
                        boolean signcourselcv = true;
                        boolean userResSign = false;
                        while signcourselcv == true {
                            signcourse = io:readln("Are you willing to sign the course outline?\n1:To sign\n2:To reject the course outline\n");
                            if signcourse == "1" {
                                signcourselcv = false;
                                userResSign = true;
                            }
                            if signcourse == "2" {
                                signcourselcv = false;
                                userResSign = false;
                            }
                        }
                        if signcourse != "" {
                            json sendSig = {opr: "upd", _type: "add_info", _data: userResSign, key: courName, user_id: check loggedOn.username};
                            io:println(sendSig);
                            json|error? sendSigres = send1(sendSig);
                        }
                    }
                    "4" => {
                        lcv = false;
                    }

                }
            }
        }
        if (loggedOn.user_type == "H.O.D") {

            while lcv == true {
                string choice = "";
                choice = io:readln("================= H.O.D Menu =======================\n1:View a course outline\n2:input information about a course\n3:Add user\n4:Add course\n5:Approve Course\n6:Logout\n");
                match choice {
                    "1" => {
                        io:println("Your course are\n", loggedOn.courses);
                        string courName = io:readln("================= Enter subject name =======================\n");
                        json courseO = {opr: "r", _type: "course", key: courName};
                        json|error? courseOutLine = send1(courseO);
                        cours = check courseOutLine;

                        io:println("The course outline for this module is: \n", cours.course_outline, "\nStatus of signature:", cours.signed);
                    }
                    "2" => {
                        string courName = io:readln("================= Enter subject name =======================\n");
                        json courseO = {opr: "r", _type: "course", key: courName};
                        json|error? courseOutLine = send1(courseO);
                        cours = check courseOutLine;
                        io:println(cours);
                        io:println("The module details are below: \n", cours.course_outline, "\nStatus of signature:", cours.approved);
                        string coursenewInfo = io:readln("Add the additional information to the course outline\n");

                        json sendSig = {_fieldToUpdate: "course_outline", opr: "upd", _type: "add_info", _data: coursenewInfo, key: courName, user_id: check loggedOn.username, user_level: check loggedOn.user_type};
                        io:println(sendSig);
                        json|error? sendSigres = send1(sendSig);

                    }
                    "3" => {
                        json user = {
                            id: uuid1String,
                            username: "David_kamo",
                            lastname: "kamo",
                            firstname: "David",
                            user_type: "student",
                            courses: [
                                {
                                    id: "95e02157126ace26414619be46f20bc9ff9c710431c523e1c246db9e",
                                    course_name: "Programming I"
                                }
                            ]
                        };
                        json d = {opr: "wr", _type: "user", data: user};
                        json|error? readResult12 = send1(d);
                    }
                    "4" => {
                        json course = {id: uuid1String, course_name: "Network Security Applications I", course_code: "NSA1223", course_outline: ["basics in Networks"]};
                        json d = {opr: "wr", _type: "course", data: course};
                        json|error? readResult12 = send1(d);
                    }
                    "5" => {
                        string courName = io:readln("================= Enter subject name =======================\n");
                        json courseO = {opr: "r", _type: "course", key: courName};
                        json|error? courseOutLine = send1(courseO);
                        cours = check courseOutLine;
                        io:println(cours);
                        io:println("The module details are below: \n", cours.course_outline, "\nStatus of Approval status:", cours.approved);
                        string aprovalStatus = "";
                        boolean aprStatus = false;
                        boolean aprStatusLcv = true;
                        while aprStatusLcv == true {
                            aprovalStatus = io:readln("Enter 1:To approve the course\nEnter 2:To reject the course\n");
                            if aprovalStatus == "1" {
                                aprStatus = true;
                                aprStatusLcv = false;
                            } else if aprovalStatus == "2" {
                                aprStatus = false;
                                aprStatusLcv = false;
                            } else {
                                io:println("Please select from the options in the menu");
                            }
                        }
                        io:println(aprovalStatus);
                        if aprovalStatus != "" {
                            json sendSig = {_fieldToUpdate: "approve_course", opr: "upd", _type: "approve_course", _data: aprStatus, key: courName, user_id: check loggedOn.username, user_level: check loggedOn.user_type};
                            io:println(sendSig);
                            json|error? sendSigres = send1(sendSig);
                        }

                    }
                    "6" => {
                        lcv = false;
                    }
                }
            }
        }
        loggedOn = {};
    }

}

public function recieve() returns error?|json {
    kafka:ConsumerRecord[] records = check consumer->poll(3);
    io:println("waiting for response");
    int a = records.length();

    int i = 0;
    foreach var kafkaRecord in records {
        if (i == (a - 1)) {
            byte[] value = kafkaRecord.value;

            string messageContent = check string:fromBytes(value);
            io:StringReader sr = new (messageContent, encoding = "UTF-8");
            //converts the message back to a json for reading and storing
            json j = check sr.readJson();
            // io:println("Received Message: " + messageContent);
            // io:println("server response in json:", j);
            return j;

        }

        i = i + 1;
    }

}

public function send() returns json|error? {

    json a = {opr: "r", key: "3", data: {}};
    io:println("Send from client:", a);
    string s = a.toJsonString();
    check kafkaProducer->send({
        topic: "reply",
        value: s.toBytes()
    });

    check kafkaProducer->'flush();
    json|error? recieveResult1 = recieve();
}

function write(json data) returns json|error? {

    io:println("Send from client:", data);
    string s = data.toJsonString();
    check kafkaProducer->send({
        topic: "reply",
        value: s.toBytes()
    });

    check kafkaProducer->'flush();
    json|error? recieveResult1 = recieve();
}

function send1(json data) returns json|error? {
    io:println("Send from client:", data);
    string s = data.toJsonString();
    check kafkaProducer->send({
        topic: "clientReq",
        value: s.toBytes()
    });

    check kafkaProducer->'flush();
    json|error? recieveResult1 = recieve();

    return recieveResult1;
}

