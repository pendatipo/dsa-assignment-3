import ballerina/io;
import ballerina/log;
import ballerinax/kafka;

string jsonFilePath = "files/course.json";
string jsonUpdateFilePath = "files/temp.json";

type Learner record {|
    string id;
    string username;
    string firstname;
    string lastname?;
    string user_type;
    json[] courses;
|};

type Course record {|
    string id;
    string course_name;
    string course_code;
    json[] course_outline;
    boolean approved;
|};

type LearnerArray Learner[];

type CourseArray Course[];

kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "1",
    topics: ["clientReq"],
    offsetReset: "earliest",
    pollingInterval: 1,
    autoCommit: false
};

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

listener kafka:Listener kafkaListener =
        new (kafka:DEFAULT_URL, consumerConfigs);

service kafka:Service on kafkaListener {
    remote function onConsumerRecord(kafka:Caller caller,
                                kafka:ConsumerRecord[] records) returns error? {
        foreach var kafkaRecord in records {
            check processKafkaRecord(kafkaRecord);
        }

        kafka:Error? commitResult = caller->commit();

        if commitResult is error {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", 'error = commitResult);
        }
    }
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns error? {
    byte[] value = kafkaRecord.value;

    string messageContent = check string:fromBytes(value);
    io:StringReader sr = new (messageContent, encoding = "UTF-8");
    //converts the message back to a json for reading and storing
    json j = check sr.readJson();
    string opera = check j.opr;
    string searchType = check j._type;
    //  io:println(opera);
    if (opera == "wr") {
        if (searchType == "user") {
            json _data = check j.data;
            error? readStudentsResult = storeUser(_data, "users");
        } else if (searchType == "course") {
            json _data = check j.data;

            error? readStudentsResult = storeCourse(_data, "courses");
        }
    }
    if (opera == "r") {
        if (searchType == "user") {
            string _id = check j.key;
            error? readStudentsResult = readusers(_id);
        } else if (searchType == "course") {
            string _id = check j.key;
            error? readStudentsResult = readcourse(_id);
        }

    }
    if (opera == "upd") {
        if (searchType == "user") {
            string _id = check j.key;
            error? readStudentsResult = readusers(_id);
        } else if (searchType == "sign_course") {
            io:println("\n======================from client:===========================\n", signed);

            string _id = check j.key;
            boolean signed = check j._data;
            string user_id = check j.user_id;

            error? readStudentsResult = signcourse(_id, signed, user_id);
        } else if (searchType == "approve_course") {
            string user_id = check j.user_id;

            string _course_id = check j.key;
            string _fieldToUpdate = check j._fieldToUpdate;
            string _data = check j._data;
            string userLevel = check j.user_level;
            io:println("===========================From Client=================================\n", j);
            error? readStudentsResult = update_course_details(_course_id, _fieldToUpdate, _data, userLevel);
        } else if (searchType == "add_info") {
            string user_id = check j.user_id;
            string _course_id = check j.key;
            string _fieldToUpdate = check j._fieldToUpdate;
            string _data = check j._data;
            string userLevel = check j.user_level;
            error? readStudentsResult = update_course_details(_course_id, _fieldToUpdate, _data, userLevel);
        }

    }

}

public function send(json msgs) returns error? {

    string message = msgs.toJsonString();
    io:println(message);
    check kafkaProducer->send({
        topic: "serverResponse",
        value: message.toBytes()
    });
    check kafkaProducer->'flush();

}

function update_course_details(string _course_id, string _fieldToUpdate, string _data, string userLevel) returns error? {
    string jsonStudentFilePath = "files/courses.json";
    map<json>[] newArr = [];
    json[] learnerCOurses = [];
    json|io:Error readJson = check io:fileReadJson(jsonStudentFilePath);

    json[] subjects = check readJson.ensureType();

    json[] arr = check readJson.ensureType();
    json rspData = {};
    Course[] CourseArray = check arr.cloneWithType();

    foreach Course value in CourseArray {
        if _course_id == value.id {
            string id = value.id;
            string course_name = value.course_name;
            learnerCOurses = value.course_outline;
            map<json> newCOurses = {

            };
            newCOurses["id"] = value.id;
            newCOurses["course_code"] = value.course_code;

            if _fieldToUpdate == "course_outline" {
                learnerCOurses.push(_data);
                newCOurses["course_outline"] = learnerCOurses;
            } else {
                newCOurses["course_outline"] = learnerCOurses;
            }
            //For approving the course outline

            if _fieldToUpdate == "approve_course" {
                newCOurses["approved"] = _data;
            } else {
                newCOurses["approved"] = value.approved;
            }

            newCOurses["course_name"] = value.course_name;

            newArr.push(newCOurses);
        } else {
            string id = value.id;
            string course_name = value.course_name;
            learnerCOurses = value.course_outline;
            map<json> newCOurses = {

            };
            newCOurses["id"] = value.id;
            newCOurses["course_code"] = value.course_code;

            newCOurses["course_outline"] = learnerCOurses;

            newCOurses["course_name"] = value.course_name;
            newCOurses["approved"] = value.approved;
            newArr.push(newCOurses);
        }

    }

    io:println("===================================Data to be stored============================");
    io:println(newArr);

    check io:fileWriteJson(jsonStudentFilePath, newArr);
}

function updatecourse(string _course_id, string _fieldToUpdate, string _data, string userLevel) returns error? {
    string jsonStudentFilePath = "files/courses.json";
    map<json>[] newArr = [];

    json[] learnerCOurses = [];
    json|io:Error readJson = check io:fileReadJson(jsonStudentFilePath);

    json[] subjects = check readJson.ensureType();

    json[] arr = check readJson.ensureType();
    json rspData = {};
    Course[] CourseArray = check arr.cloneWithType();

    foreach Course value in CourseArray {
        string id = value.id;
        string courName = value.course_name;
        if (id == _course_id || courName == _course_id) {
            string course_name = value.course_name;
            string? course_code = value?.course_name;
            if course_name is string {
                course_name = course_name;
            }
            learnerCOurses = value.course_outline;

            map<json> newCOurses = {
                course_name
            };
            newCOurses["id"] = value.id;
            newCOurses["course_code"] = value.course_code;

            if _fieldToUpdate == "course_outline" {
                learnerCOurses.push(_data);
                newCOurses["course_outline"] = learnerCOurses;
            } else {
                newCOurses["course_outline"] = learnerCOurses;
            }
            if _fieldToUpdate == "approved" {
                if userLevel == "H.O.D" {
                    newCOurses["approved"] = _data;
                } else {
                    newCOurses["approved"] = value.approved;
                }
            }

            newArr.push(newCOurses);
            rspData = newCOurses;
        } else {
            newArr.push(value);
        }

    }

    check io:fileWriteJson("files/courses146.json", newArr);
    //error? sendResult = send(rspData);
}

function signcourse(string _id, boolean signed, string uid) returns error? {
    io:println("are you willing to sign the course?", _id, " sign=", signed);
    string jsonStudentFilePath = "files/users.json";
    map<json>[] newArr = [];
    json[] learnerCOurses = [];
    json[] signedCourses = [];
    json|io:Error readJson = check io:fileReadJson(jsonStudentFilePath);

    json[] subjects = check readJson.ensureType();
    Course signCourse;
    json[] arr = check readJson.ensureType();
    json rspData = {};
    Learner[] LearnerArr = check arr.cloneWithType();

    foreach Learner value in LearnerArr {
        string id = value.id;
        string usersName = value.username;
        json update = {};
        if (id == uid || usersName == uid) {
            string name = value.firstname;
            string? lastName = value?.lastname;
            if lastName is string {
                name += " " + lastName;
            }
            learnerCOurses = value.courses;
            map<json> newLearner = {

            };
            foreach var item in learnerCOurses {
                io:println(item);
                if item.id == _id {
                    update = {id: check item.id, course_name: check item.course_name, signed: signed};
                    signedCourses.push(update);
                }
                else {

                    signedCourses.push(item);
                }
            }

            newLearner["id"] = value.id;
            newLearner["username"] = value.username;
            newLearner["lastname"] = value?.lastname;
            newLearner["firstname"] = value.firstname;
            newLearner["user_type"] = value.user_type;
            newLearner["courses"] = signedCourses;

            rspData = {newLearner};
            newArr.push(newLearner);
        } else {
            string name = value.firstname;
            string? lastName = value?.lastname;
            if lastName is string {
                name += " " + lastName;
            }
            learnerCOurses = value.courses;
            map<json> newLearner = {

            };

            newLearner["id"] = value.id;
            newLearner["username"] = value.username;
            newLearner["lastname"] = value?.lastname;
            newLearner["firstname"] = value.firstname;
            newLearner["user_type"] = value.user_type;
            newLearner["courses"] = learnerCOurses;
            newArr.push(newLearner);
        }

    }
    io:println("==================================================\n", newArr, "\n=========================================================");
    check io:fileWriteJson(jsonStudentFilePath, newArr);
    error? sendResult = send(rspData);
}

function readusers(string user_id) returns error? {
    string jsonStudentFilePath = "files/users.json";
    map<json>[] newArr = [];
    json[] learnerCOurses = [];
    json|io:Error readJson = check io:fileReadJson(jsonStudentFilePath);

    json[] subjects = check readJson.ensureType();

    json[] arr = check readJson.ensureType();
    json rspData = {};
    Learner[] LearnerArr = check arr.cloneWithType();

    foreach Learner value in LearnerArr {
        string id = value.id;
        string usersName = value.username;
        io:println(id);
        if (id == user_id || usersName == user_id) {
            string name = value.firstname;
            string? lastName = value?.lastname;
            if lastName is string {
                name += " " + lastName;
            }
            learnerCOurses = value.courses;
            map<json> newLearner = {
                name
            };
            newLearner["id"] = value.id;
            newLearner["username"] = value.username;
            newLearner["user_type"] = value.user_type;
            newLearner["courses"] = learnerCOurses;

            rspData = newLearner;
        }

    }
    error? sendResult = send(rspData);

}

function readcourse(string course_id) returns error? {
    string jsonStudentFilePath = "files/courses.json";
    map<json>[] newArr = [];
    json[] learnerCOurses = [];
    json|io:Error readJson = check io:fileReadJson(jsonStudentFilePath);

    json[] subjects = check readJson.ensureType();

    json[] arr = check readJson.ensureType();
    json rspData = {};
    Course[] CourseArray = check arr.cloneWithType();

    foreach Course value in CourseArray {
        string id = value.id;
        string courName = value.course_name;
        if (id == course_id || courName == course_id) {
            string course_name = value.course_name;
            string? course_code = value?.course_name;
            if course_name is string {
                course_name = course_name;
            }
            learnerCOurses = value.course_outline;
            map<json> newCOurses = {
                course_name
            };
            newCOurses["id"] = value.id;
            newCOurses["course_code"] = value.course_code;
            newCOurses["course_outline"] = learnerCOurses;
            newCOurses["approved"] = value.approved;

            rspData = newCOurses;
        }

    }
    error? sendResult = send(rspData);

}

function storeCourse(json data, string fname) returns error? {
    string jsonStudentFilePath = "files/courses.json";
    map<json>[] newArr = [];
    json[] learnerCOurses = [];
    json|io:Error readJson = check io:fileReadJson(jsonStudentFilePath);

    json[] subjects = check readJson.ensureType();

    json[] arr = check readJson.ensureType();
    json rspData = {};
    Course[] CourseArray = check arr.cloneWithType();

    foreach Course value in CourseArray {
        string id = value.id;
        string course_name = value.course_name;
        learnerCOurses = value.course_outline;
        map<json> newCOurses = {

        };
        newCOurses["id"] = value.id;
        newCOurses["course_code"] = value.course_code;
        newCOurses["course_outline"] = learnerCOurses;
        newCOurses["course_name"] = value.course_name;
        newArr.push(newCOurses);

    }
    json jsonContent = data;
    newArr.push(<map<json>>jsonContent);
    io:println("===================================Data to be stored============================");
    io:println(newArr);
    string finame = fname + ".json";
    string jsonFilePath = "./files/" + finame;

    check io:fileWriteJson(jsonStudentFilePath, newArr);

}

function storeUser(json data, string fname) returns error? {

    string jsonStudentFilePath = "files/users.json";
    map<json>[] newArr = [];
    json[] learnerCOurses = [];
    json|io:Error readJson = check io:fileReadJson(jsonStudentFilePath);

    json[] subjects = check readJson.ensureType();

    json[] arr = check readJson.ensureType();

    Learner[] LearnerArr = check arr.cloneWithType();

    foreach Learner value in LearnerArr {
        string id = value.id;
        io:println(id);

        string? lastName = value?.lastname;

        learnerCOurses = value.courses;
        map<json> newLearner = {

        };
        newLearner["id"] = value.id;
        newLearner["firstname"] = value.firstname;
        newLearner["username"] = value.username;
        newLearner["courses"] = learnerCOurses;
        newLearner["user_type"] = value.user_type;
        newArr.push(newLearner);
    }
    json jsonContent = data;
    newArr.push(<map<json>>jsonContent);
    io:println("===================================Data to be stored============================");
    io:println(newArr);
    string finame = fname + ".json";
    string jsonFilePath = "./files/" + finame;

    check io:fileWriteJson(jsonStudentFilePath, newArr);

}
